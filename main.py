import wx
from re import split
from pynput.keyboard import Controller, Key

keyboard = Controller()

class MainFrame(wx.Frame):
    def __init__(self, *args, **kw):
        super(MainFrame, self).__init__(*args, **kw)
        self.build()

    def build(self):
        #layout
        box = wx.BoxSizer(orient=wx.VERTICAL)
        self.box = box
        self.create_inputs()

        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.simulate_press_keys)
        
    def create_inputs(self):
        panel       = wx.Panel(self, -1)
        
        lb_interval = wx.StaticText(panel, label="Intervalo (em segundos):")
        tc_interval = wx.TextCtrl(panel)
        lb_keys = wx.StaticText(panel, label="Valor a ser digitado (use {tecla} para teclas especiais, separados por linhas)")
        tc_keys     = wx.TextCtrl(panel, style=wx.TE_MULTILINE|wx.EXPAND)
        bt_start    = wx.Button(panel, label="Iniciar")

        bt_start.Bind(wx.EVT_LEFT_DOWN, self.on_click_bt_start)

        self.box.Add(lb_interval, flag=wx.RIGHT|wx.LEFT|wx.TOP, border=10)
        self.box.Add(tc_interval, flag=wx.EXPAND|wx.ALL, border=10)
        self.box.Add(lb_keys, flag=wx.RIGHT|wx.LEFT|wx.TOP, border=10)
        self.box.Add(tc_keys, flag=wx.EXPAND|wx.ALL, border=10)
        self.box.Add(bt_start, flag=wx.ALIGN_RIGHT|wx.ALL, border=10)

        panel.SetSizer(self.box)

        self.tc_interval = tc_interval
        self.tc_keys = tc_keys
        self.bt_start = bt_start

    def on_click_bt_start(self, event):

        if self.timer.IsRunning():
            self.enable_controls(True)
            self.timer.Stop()
            return True
        
        try: 
            interval = 1000 * float(self.tc_interval.GetValue())
            self.timer.Start(interval)
            self.enable_controls(False)
        except: 
            wx.MessageBox(
                "O campo intervalo deve ser um número inteiro ou fracionado",
                style=wx.ICON_EXCLAMATION
            )
    
    def simulate_press_keys(self, event):

        value = self.tc_keys.GetValue()
        
        for part in value.split("\n"):
            if part.startswith('{/') and part.endswith('}') and hasattr(Key, part[2:-1]):
                keyboard.release(getattr(Key, part[2:-1]))
            elif part.startswith('{') and part.endswith('}') and hasattr(Key, part[1:-1]):
                keyboard.press(getattr(Key, part[1:-1]))
            else:
                keyboard.type(part)

    def enable_controls(self, editable=False):
        self.tc_interval.SetEditable(editable)
        self.tc_keys.SetEditable(editable)
        self.bt_start.SetLabel("Iniciar" if editable else "Parar")


def main(*args):
    app = wx.App()
    mf = MainFrame(None, title="Maxters - Autokeys", size=(500, 250))
    mf.Show()
    app.MainLoop()


if __name__ == "__main__":
    from sys import argv
    main(*argv)